console.log('page load - entered main.js for js api functions');

var submit_button = document.getElementById('bsr-submit-button');
submit_button.onmouseup = get_form_info;

function get_form_info()
{
    console.log('entered function get_form_info');
    var state = document.getElementById("input-text").value;
    console.log('entered ' + state + ' state');

    make_net_call_to_brewery_api(state);
    make_net_call_to_activity_api();
}

function make_net_call_to_brewery_api(state)
{
    console.log('entered function make_net_call_to_brewery_api ' + state);
    var xhr = new XMLHttpRequest();
    var url = "https://api.openbrewerydb.org/breweries?by_state=" + state;
    console.log(url);
    xhr.open("GET", url, true);

    xhr.onload = function(e)
    {
        console.log(xhr.responseText);
        update_html_with_brews(state, xhr.responseText);
    }

    xhr.onerror = function(e)
    {
        console.error(xhr.statusText);
    }

    xhr.send(null);
}

function make_net_call_to_activity_api()
{
    console.log('entered function to make network call to activity API');
    var xhr = new XMLHttpRequest();
    url = "https://www.boredapi.com/api/activity/";
    console.log(url);

    xhr.open("GET", url, true);

    xhr.onload = function(e)
    {
        console.log(xhr.responseText);
        update_html_with_activity(xhr.responseText);
    }

    xhr.onerror = function(e)
    {
        console.error(xhr.statusText);
    }

    xhr.send(null);

}

function update_html_with_activity(response_text)
{
    var response_json = JSON.parse(response_text);
    var label1 = document.getElementById("second_api");

    label1.innerHTML = response_json['activity'];
}

function remove_children(label)
{
    while(label.firstChild){
        label.removeChild(label.firstChild);
    }
}

function update_html_with_brews(state, response_text)
{
    var response_json = JSON.parse(response_text);
    var label1 = document.getElementById("first_api");

    if (label1.hasChildNodes()){
        remove_children(label1);
    }
    
    for (i = 0; i < Object.keys(response_json).length; i++){
        var entry = document.createElement('li');
        entry.appendChild(document.createTextNode(response_json[i]['name']));
        label1.appendChild(entry);
    }
}